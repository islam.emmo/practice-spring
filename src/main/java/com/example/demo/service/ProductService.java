package com.example.demo.service;

import java.util.ArrayList;

import com.example.demo.dao.ProductRepository;
import com.example.demo.model.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    Product product;

    public ProductService(Product newProduct) {
        product = newProduct;
    }

    public ArrayList<Product> getAllProducts() {
        return (ArrayList<Product>) repository.findAll();
    }

    public String getProductName() {
        product.setName("islam");
        return product.getName();
    }
}
