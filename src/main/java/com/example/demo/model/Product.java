package com.example.demo.model;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "product")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class Product implements Serializable, DisposableBean {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "name")
    @Value("${person.name}")
    private String name;

    // @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    // public Product getProduct() {
    // return new Product();
    // }

    public String getDailyWorkout() {
        return "getDailyWorkout";
    }

    @PostConstruct
    public void doStartUpStuff() {
        System.out.println("Initialization product bean");
    }

    @PreDestroy
    public void doCleanUpStuff() {
        System.out.println("Destory product bean");

    }

    @Override
    public void destroy() throws Exception {
        // TODO Auto-generated method stub
        doCleanUpStuff();

    }
}
