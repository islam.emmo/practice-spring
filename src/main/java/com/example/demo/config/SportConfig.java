package com.example.demo.config;

import com.example.demo.model.Product;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;

import net.bytebuddy.dynamic.loading.ClassLoadingStrategy.Configurable;

@Configuration
@PropertySource("classpath:sport.properties")
@ComponentScan
public class SportConfig {

    // define bean for our servcice
    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public Product getReadyProduct() {
        return new Product();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Product getProtoProduct() {
        return new Product();
    }
}
