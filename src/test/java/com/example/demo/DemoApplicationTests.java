package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import com.example.demo.config.SportConfig;
import com.example.demo.model.Product;
import com.example.demo.service.OrderService;
import com.example.demo.service.ProductService;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootTest
public class DemoApplicationTests {
	@Autowired
	private OrderService orderService;

	@Autowired
	private ProductService productService;

	@Test
	public void contextLoads() {
	}

	final String NAME = "Islam Emam";

	@Test
	public void configInJava() {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);

		for (String beanName : context.getBeanDefinitionNames()) {
			System.out.println(beanName);
		}

		// Product productA = context.getBean("product", Product.class);

		// Product productB = context.getBean("product", Product.class);

		// System.out.println("Hashcode");
		// System.out.println(productA.hashCode());

		// System.out.println(productB.hashCode());
	}

	@Test
	public void insertProductToDb() {

		// Create Application context container
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);

		// create session factory
		SessionFactory factory = new Configuration().configure().addAnnotatedClass(Product.class).buildSessionFactory();

		// Get current session to comminucate with db
		Session session = factory.getCurrentSession();

		Product newProduct = context.getBean("getProtoProduct", Product.class);

		newProduct.setName("Coca cola3");

		try {

			// begin transaction
			session.beginTransaction();

			// save object to db
			session.save(newProduct);

			// commit transaction
			session.getTransaction().commit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			factory.close();
		}

	}

	@Test
	public void configHibernate() {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Product.class)
				.buildSessionFactory();

		Session session = factory.getCurrentSession();

		Product product = Product.builder().name("pepsi").build();

		try {

			// start transaction
			session.beginTransaction();

			// save object
			session.save(product);

			// session.createQuery("from Product p where p.name = 'Doe'").getResultList();

			// commit the transaction
			session.getTransaction().commit();

		} catch (Exception e) {

		} finally {
			factory.close();
		}

	}

	@Test
	public void beansLifecycle() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("scopes.xml");
		Product product = context.getBean("myProduct", Product.class);
		System.out.println("Name is " + product.toString());
		context.close();
	}

	@Test
	public void isProductAEqualsProductB() {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("scopes.xml");

		Product personSingletonA = (Product) applicationContext.getBean("myProduct");

		personSingletonA.setName(NAME);

		Product personB = personSingletonA;

		ProductService service = applicationContext.getBean("productService", ProductService.class);

		System.out.println(service.getProductName());
		assertEquals(NAME, personSingletonA.getName());

		//

	}

	@Test
	public void whenUserIdIsProvided_thenRetrievedNameIsCorrect() {
		// Mockito.when(productService.getProductName()).thenReturn("Mock Product
		// Name");
		// String testName = orderService.getProductName();

		assertTrue("islam".equals("ahmed"));
	}

}
